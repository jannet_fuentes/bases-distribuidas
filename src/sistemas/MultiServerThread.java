import java.net.*;
import java.io.*;
import java.util.Date;
import javax.swing.JOptionPane;

public class MultiServerThread extends Thread {
   private Socket socket = null;
   private String tabla= "abcdefghijklmnñopqrstuvwxyzáéíóúABCDEFGHIJKLMNÑOPQRSTUVW"
                 + "XYZÁÉÍÓÚ1234567890.,;_:+-*/ @$#¿?!Â¡=()[]{}\\\"";

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	     while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("mayusculas")){
               String a="",b="";
        a=JOptionPane.showInputDialog(null,"Ingrese la frase o palabra");
        //b=a.toLowerCase(); //para cnvertir a minusculas
        b=a.toUpperCase(); //para convertir a mayusculas
        escritor.println(""+b);
			   }else{ if(lineIn.equals("minusculas")){
               String a="",b="";
        a=JOptionPane.showInputDialog(null,"Ingrese la frase o palabra");
        b=a.toLowerCase(); //para cnvertir a minusculas
        //b=a.toUpperCase(); //para convertir a mayusculas
        escritor.println(""+b);
                           }else{ if(lineIn.equals("inversion")){
               String a="",b="";
        a=JOptionPane.showInputDialog(null,"Ingrese la frase o palabra");
        char [] invertir=a.toCharArray();
        int cont;
        for(cont=a.length()-1;cont>=0;cont--){
           b=" "+b+" "+invertir[cont];
        }
        escritor.println(" "+b);
                           }else{ if(lineIn.equals("binario")){
               String a="",b="";
        a=JOptionPane.showInputDialog(null,"Ingrese el numero decimal");
        int decimal,modulo;
        String binario="";
        decimal=Integer.parseInt(a);
        while (decimal>0){
            modulo=(decimal%2);
            binario=modulo + binario;
            decimal=decimal/2;
        }
        escritor.println("El numero  "+decimal+" BASE 10 es El numero en "
                + "binaro es "+binario);
                           }else{ if(lineIn.equals("encriptar")){
               String a="",b="";
        a=JOptionPane.showInputDialog(null,"Ingrese la frase");
        b=Encriptar(a,3);
        escritor.println(" "+b);
                           }else{ if(lineIn.equals("desencriptar")){
               String a="",b="";
        a=JOptionPane.showInputDialog(null,"Ingrese la frase");
        b=Desencriptar(a,3);
        escritor.println(" "+b);
                           }else{ if(lineIn.equals("fin")){
               ServerMultiClient.NoClients--;
                               break;
                               }else{
               escritor.println("Servicio no registrado");
               escritor.flush();
            }
         }
             }
             }
             }
             }
             }
             }
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   private String Limpiar_texto(String texto)
     {
         texto = texto.replaceAll("\n", "");  
         
        for(int x = 0; x < texto.length(); x++)
        {
            int posicion = tabla.indexOf(texto.charAt(x));
            
            if (posicion == -1)
            {
                texto = texto.replace(texto.charAt(x), ' ');
            }
        }        
        return texto;
    }


    public  String Encriptar(String texto, int clave)
        {       
        String texto_limpio = Limpiar_texto(texto);
        
        String resultado = "";  
        
     for(int i = 0; i < texto_limpio.length();i++)
        {
           int posicion = tabla.indexOf(texto_limpio.charAt(i));
           
           if ((posicion + clave ) < tabla.length())
            {
                resultado += tabla.charAt(posicion + clave);
            }
            else
            {
                resultado += tabla.charAt((posicion + clave) - tabla.length());
            }         
        }        
        return resultado;
    }




public String Desencriptar(String texto, int clave)
    {        
        String texto_limpio = Limpiar_texto(texto);
        
        String resultado = "";   
        
        for(int x = 0; x < texto_limpio.length(); x++)
        {            
            int posicion = tabla.indexOf(texto_limpio.charAt(x)); 
            
            if ((posicion - clave) < 0)
            {
                resultado +=  tabla.charAt((posicion - clave) + tabla.length());
            }
            else
            {
                resultado +=  tabla.charAt(posicion - clave);
            }         
        }        
        return resultado;
    }
} 
